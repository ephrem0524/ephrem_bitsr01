
// Get the basic details section element
const basicSection = document.querySelector('#basic-details');

// Add a mouseover event listener to the basic details section to change the font size
basicSection.addEventListener('mouseover', () => {
basicSection.style.fontSize = '1.1em';
});
const bioSections = document.querySelector('#bio');

// Add a mouseover event listener to the bio section to change the font size
bioSections.addEventListener('mouseover', () => {
 bioSections.style.fontSize = '1.1em';
});
const educationSection = document.querySelector('#education');

// Add a mouseover event listener to the education section to change the font size
educationSection.addEventListener('mouseover', () => {
  educationSection.style.fontSize = '1.1em';
});
const skillSection = document.querySelector('#skills');

// Add a mouseover event listener to the education section to change the font size
skillSection.addEventListener('mouseover', () => {
  skillSection.style.fontSize = '1.1em';
});

// Get the bio section element
const bioSection = document.querySelector('#bio');

// Add a scroll event listener to the window to toggle the visibility of the bio section
window.addEventListener('scroll', () => {
  const bioSectionTop = bioSection.getBoundingClientRect().top;
  if (bioSectionTop < window.innerHeight) {
    bioSection.style.display = 'block';
  } else {
    bioSection.style.display = 'none';
  }
});


